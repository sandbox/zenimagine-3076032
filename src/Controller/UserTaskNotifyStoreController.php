<?php

namespace Drupal\user_task_notify\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_store\Entity\StoreInterface;

class UserTaskNotifyStoreController extends ControllerBase {

  public function Tasks() {
    return [
      '#theme' => 'user_task_notify_store_template',
    ];
  }

  public function taskAccess(StoreInterface $store, AccountInterface $account) {
    $result = $store->access('edit', $account, TRUE);
    return AccessResult::isAllowed();
  }

}