<?php

namespace Drupal\user_task_notify\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupInterface;

class UserTaskNotifyGroupController extends ControllerBase {

  public function Tasks() {
    return [
      '#theme' => 'user_task_notify_group_template',
    ];
  }

  public function taskAccess(AccountInterface $account) {
    $uid = $this->route_match->getParameter('user');
    return AccessResult::allowedIf($account->id() == $uid)
      ->orIf(AccessResult::allowedIfHasPermission($account, 'administer group settings'));
  }

}